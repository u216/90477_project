# 90477_project/data

This folder contains the data that were used in this project. 

This data are available on the famous repository "PROMISE" ((http://promise.site.uottawa.ca/SERepository/datasets-page.html)) with title "CM1/Software defect prediction". 

Attribute Information in the original data:

1. **loc**               : numeric % McCabe's line count of code
Number of line of code

2. **v(g)**              : numeric % McCabe "cyclomatic complexity" 
Is a measure of the complexity of a module's decision structure. It is the number of linearly independent paths and therefore, the minimum number of paths that should be tested.

3. **ev(g)**             : numeric % McCabe "essential complexity"
Is a measure of the degree to which a module contains unstructured constructs. This metric measures the degree of structuredness and the quality of the code. It is used to predict the maintenance effort and to help in the modularization process.

4. **iv(g)**             : numeric % McCabe "design complexity"
Is the complexity of the design-reduced module and reflects the complexity of the module's calling patterns to its immediate subordinate modules. This metric differentiates between modules which will seriously complicate the design of any program they are part of and modules which simply contain complex computational logic. It is the basis upon which program design and integration complexities (S0 and S1) are calculated.

5. **n**                 : numeric % Halstead total operators + operands


6. **v**                 : numeric % Halstead "volume"
The minimum number of bits required for coding the program.

7. **l**                 : numeric % Halstead "program length"
The total number of operator occurrences and the total number of operand occurrences.

8. **d**                 : numeric % Halstead "difficulty"
Measure the program's ability to be comprehended.

9. **i**                 : numeric % Halstead "intelligence"
Shows the complexity of a given algorithm independent of the language used to express the algorithm.

10. **e**                 : numeric % Halstead "effort"
The estimated mental effort required to develop the program.

11. **b**                 : numeric % Halstead 

12. **t**                 : numeric % Halstead's time estimator
The estimated amount of time to implement an algorithm.

13. **lOCode**            : numeric % Halstead's line count

14. **lOComment**         : numeric % Halstead's count of lines of comments

15. **lOBlank**           : numeric % Halstead's count of blank lines

16. **lOCodeAndComment**  : numeric

17. **uniq_Op**           : numeric % unique operators

18. **uniq_Opnd**         : numeric % unique operands

19. **total_Op**          : numeric % total operators

20. **total_Opnd**        : numeric % total operands

21. **branchCount**       : numeric % of the flow graph

22. **defects**           : {false,true} % module has/has not one or more reported defects

