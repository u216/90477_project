# 90477_project
**Maya Pino** (0001026186)

Project for the exam of the course "System and Algorithms for Data Science".
Project number **15**

"_Software Complexity Prediction_"
Original link of the research: "[text](https://ijaers.com/detail/software-complexity-prediction-by-using-basic-attributes-2/)"

The project is divided in 5 folders:

- **scripts**; contain python scripts and jupyter notebook files
- **con**; contains configuration files used
- **data**; contains input files
- **results**; contains .csv files
- **figures**; contains pllot files
