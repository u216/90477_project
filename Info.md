"http://promise.site.uottawa.ca/SERepository/datasets/cm1.arff"

These features were defined in the 70s in an attempt
%    to objectively characterize code features that are associated with
%    software quality. The McCabe and Halstead measures are "module"-based where a
%    "module" is the smallest unit of functionality. In C or Smalltalk,
%    "modules" would be called "function" or "method" respectively.

Defect detectors can be assessed according to the following measures:
%
%                                                module actually has defects
%                                               +-------------+------------+
%                                               |     no      |     yes    |
%                                         +-----+-------------+------------+
%          classifier predicts no defects |  no |      a      |      b     |
%                                         +-----+-------------+------------+
%        classifier predicts some defects | yes |      c      |      d     |
%                                         +-----+-------------+------------+
%
%        accuracy                   = acc          = (a+d)/(a+b+c+d
%        probability of detection   = pd  = recall = d/(b+d)
%        probability of false alarm = pf           = c/(a+c)
%        precision                  = prec         = d/(c+d)
%        effort                     = amount of code selected by detector
%                                   = (c.LOC + d.LOC)/(Total LOC)

Ideally, detectors have high PDs, low PFs, and low
%        effort. This ideal state rarely happens:
%
%        -- PD and effort are linked. The more modules that trigger
%        the detector, the higher the PD. However, effort also gets
%        increases
%
%        -- High PD or low PF comes at the cost of high PF or low PD
%        (respectively). This linkage can be seen in a standard
%        receiver operator curve (ROC).  Suppose, for example, LOC> x
%        is used as the detector (i.e. we assume large modules have
%        more errors). LOC > x represents a family of detectors. At
%        x=0, EVERY module is predicted to have errors. This detector
%        has a high PD but also a high false alarm rate. At x=0, NO
%        module is predicted to have errors. This detector has a low
%        false alarm rate but won't detect anything at all. At 0<x<1,
%        a set of detectors are generated as shown below:
%
%                 pd
%               1 |           x  x  x                KEY:
%                 |        x     .                   "."  denotes the line PD=PF
%                 |     x      .                     "x"  denotes the roc curve 
%                 |   x      .                            for a set of detectors
%                 |  x     .
%                 | x    . 
%                 | x  .
%                 |x .
%                 |x
%                 x------------------ pf    
%                0                   1
%
%         Note that:
%
%         -- The only way to make no mistakes (PF=0) is to do nothing
%        (PD=0)
%
%         -- The only way to catch more detects is to make more
%         mistakes (increasing PD means increasing PF).
%
%         -- Our detector bends towards the "sweet spot" of
%         <PD=1,PF=0> but does not reach it.
%
%         -- The line pf=pd on the above graph represents the "no information"
%         line. If pf=pd then the detector is pretty useless. The better
%         the detector, the more it rises above PF=PD towards the "sweet spot".
%
 -- USEFUL: E.g. this data set can generate highly accurate
%     predictors for defects  
% 
%     -- EASY TO USE: Static code measures (e.g. lines of code, the
%     McCabe/Halstead measures) can be automatically and cheaply
%     collected.
% 
%     -- WIDELY USED: Many researchers use static measures to guide
%     software quality predictions (see the reference list in the above
%     "blind spot" paper. Verification and validation (V\&V) textbooks
%     advise using static code complexity measures to decide which
%     modules are worthy of manual inspections.  Further, we know of
%     several large government software contractors that won't review
%     software modules _unless_ tools like McCabe predict that they are
%     fault prone.  Hence, defect detectors have a major economic impact
%     when they may force programmers to rewrite code. Nevertheless, the merits of these metrics has been widely
%     criticized.  Static code measures are hardly a complete
%     characterization of the internals of a function.

The McCabe metrics are a collection of four software metrics:
%     essential complexity, cyclomatic complexity, design complexity and
%     LOC, Lines of Code.
% 
%     -- Cyclomatic Complexity, or "v(G)", measures the number of
%     "linearly independent paths". A set of paths is said to be 
%     linearly independent if no path in the set is a linear combination
%     of any other paths in the set through a program's "flowgraph". A
%     flowgraph is a directed graph where each node corresponds to a
%     program statement, and each arc indicates the flow of control from
%     one statement to another. "v(G)" is calculated by "v(G) = e - n + 2"
%     where "G" is a program's flowgraph, "e" is the number of arcs in
%     the flowgraph, and "n" is the number of nodes in the
%     flowgraph. The standard McCabes rules ("v(G)">10), are used to 
%     identify fault-prone module.
% 
%     -- Essential Complexity, or "ev(G)$" is the extent to which a
%     flowgraph can be "reduced" by decomposing all the subflowgraphs
%     of $G$ that are "D-structured primes". Such "D-structured
%     primes" are also sometimes referred to as "proper one-entry
%     one-exit subflowgraphs" (for a more thorough discussion of
%     D-primes, see the Fenton text referenced above). "ev(G)" is 
%     calculated using "ev(G)= v(G) - m" where $m$ is the number of 
%     subflowgraphs of "G" that are D-structured primes.
% 
%     -- Design Complexity, or "iv(G)", is the cyclomatic complexity of a
%     module's reduced flowgraph.  The flowgraph, "G", of a module is
%     reduced to eliminate any complexity which does not influence the
%     interrelationship between design modules.  According to McCabe,
%     this complexity measurement reflects the modules calling patterns 
%     to its immediate subordinate modules.
% 
%     -- Lines of code is measured according to McCabe's line counting 
%     conventions.
% 
%     The Halstead falls into three groups: the base measures, the
%     derived measures, and lines of code measures.
%     
%     -- Base measures: 
%        -- mu1             = number of unique operators
%        -- mu2             = number of unique operands
%        -- N1              = total occurrences of operators
%        -- N2              = total occurrences of operands
%        -- length     = N  = N1 + N2
%        -- vocabulary = mu = mu1 + mu2
%        -- Constants set for each function:
%           -- mu1' =  2 = potential operator count (just the function
%                          name and the "return" operator)
%           -- mu2'      = potential operand count. (the number 
%                          of arguments to the module)
%        
%        For example, the expression "return max(w+x,x+y)" has "N1=4"
%        operators "return, max, +,+)", "N2=4" operands (w,x,x,y),
%        "mu1=3" unique operators (return, max,+), and "mu2=3" unique
%        operands (w,x,y).
% 
%      -- Derived measures:
%         -- P = volume = V = N * log2(mu) (the number of mental 
%                                           comparisons needed to write
%                                           a program of length N)
%         -- V* = volume on minimal implementation
%               = (2 + mu2')*log2(2 + mu2')
%         -- L  = program length = V*/N
%         -- D  = difficulty = 1/L
%         -- L' = 1/D 
%         -- I  = intelligence = L'*V'
%         -- E  = effort to write program = V/L 
%         -- T  = time to write program = E/18 seconds





















